package com.allianz.onegdn.topCoder.model.persistence;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class UserProfile extends BaseEntity{
	
	@Column
	private String firstName;
	
	@Column
	private String lastName;
	
	@Column
	private String email;
	
	@Column
	private String userType;
	
	@Column
	private Integer grade;
	
	@Column
	private Integer questionsPosted;
	
	@Column
	private Integer questionsAnswered;
	
	@Column
	private Integer verifiedAnswers;
	
	//private List<RecognitionDTO> recognitions;
	

}
