package com.allianz.onegdn.topCoder.model;

/**
 * @author Amjith K
 *
 */
public class TechnologiesDTO {
	private String id;
	private String name;
	private Integer answeredQuestions;
	private Integer unansweredQuestions;
	private UserProfileDTO topContributer;

}
