package com.allianz.onegdn.topCoder.model;

import java.util.Date;
import java.util.List;

/**
 * @author Amjith K
 *
 */
public class QuestionDTO {
	private String id;
	private String questionTitle;
	private TechnologiesDTO technology;
	private String postedBy;//userProfile id
	private Date postedDate;
	private String status;//Open/Closed (Enum/Constant class preferable)
	
	private List<AnswerDTO> answers;
	

}
