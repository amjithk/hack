package com.allianz.onegdn.topCoder.model.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Question extends BaseEntity{
	
	@Lob
	@Column(length = 1000000, name = "question_title")
	protected String questionTitle;
	
	@Column
	protected String status;
	
	@Column
	protected Date postedDate;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="user_profile_id")
	protected UserProfile postedBy;//userProfile id
	
	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "question", targetEntity = Answer.class)
	protected List<Answer> answers= new ArrayList<Answer>();
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="technology_id")
	protected Technologies technology;
	

}
