package com.allianz.onegdn.topCoder.model;

import java.util.List;

/**
 * @author Amjith K
 *
 */
public class UserProfileDTO {
	
	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private String userType;
	private int grade;
	
	private int questionsPosted;
	private int questionsAnswered;
	private int verifiedAnswers;
	
	private List<RecognitionDTO> recognitions;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public int getQuestionsPosted() {
		return questionsPosted;
	}

	public void setQuestionsPosted(int questionsPosted) {
		this.questionsPosted = questionsPosted;
	}

	public int getQuestionsAnswered() {
		return questionsAnswered;
	}

	public void setQuestionsAnswered(int questionsAnswered) {
		this.questionsAnswered = questionsAnswered;
	}

	public int getVerifiedAnswers() {
		return verifiedAnswers;
	}

	public void setVerifiedAnswers(int verifiedAnswers) {
		this.verifiedAnswers = verifiedAnswers;
	}

	public List<RecognitionDTO> getRecognitions() {
		return recognitions;
	}

	public void setRecognitions(List<RecognitionDTO> recognitions) {
		this.recognitions = recognitions;
	}
	
	
	
	
	

}
