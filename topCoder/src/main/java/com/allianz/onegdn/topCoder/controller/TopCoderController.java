package com.allianz.onegdn.topCoder.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allianz.onegdn.topCoder.model.UserProfileDTO;

/**
 * @author Amjith K
 *
 */
@RestController
public class TopCoderController extends BaseController{
	
	@RequestMapping("/test")
    public UserProfileDTO test() {
		UserProfileDTO up= new UserProfileDTO();
		up.setId("1");
		up.setEmail("amj@al.com");
		up.setFirstName("Amjith");
		up.setLastName("K");
		return up;
	}
	
	

}
