package com.allianz.onegdn.topCoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Amjith K
 *
 */
@SpringBootApplication
public class TopCoderApplication {

	public static void main(String[] args) {
		System.out.println("Application Up and running");
		SpringApplication.run(TopCoderApplication.class, args);
		
	}

}
