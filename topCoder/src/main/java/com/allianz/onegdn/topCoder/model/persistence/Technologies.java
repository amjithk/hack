package com.allianz.onegdn.topCoder.model.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @author wk8cvxg
 *
 */
@Entity
public class Technologies extends BaseEntity{
	
	@Column
	protected String name;
	
	@Column
	protected Integer answeredQuestions;
	
	@Column
	protected Integer unansweredQuestions;
	
	//@OneToOne
	//@JoinColumn(name = "user_profile_id")
	protected UserProfile topContributer;

}
